# Changes
This is a list of changes to so Christopher can know the steps that I took to make this project typescript compliant.


## 2023-04-01
1. Forked to christopher's fkm repo to `nelson-christ` group to aid christopher without having access to push to his code

2. Move the `LinkFormattingHelper.js` file to `src`

3. Perform a `npm init`

4. Copy the `devDependencies` and a bit of the `scripts` sections from https://github.com/nelson-perez/large-sort/blob/main/package.json

5. Copy https://github.com/nelson-perez/large-sort/blob/main/tsconfig.json

6. Rename `LinkFormattingHelper.js` to `LinkFormattingHelper.ts`

7. Made the `javascript` to `typescript`
   a. Changed the `require()` to `import` statements.
   b. Made all the function parameters to typed like `dirPath: string` and `filePath: string`
